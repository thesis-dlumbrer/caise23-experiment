# Installation

No installation process is required to view the scenes. Simply deploy them (refer to the **[README.md/Scenes](./README.md#deploy-it-locally)** section) or visit **[the permanent link](https://thesis-dlumbrer.gitlab.io/vissoft23-experiment)**.

For additional information regarding results or replicating the analysis, please consult the details provided in the **[README.md](./README.md)** file.
