# Visualizing NPM dependencies

# 1. Generate dependencies list

## 1.1 Install your package/application

```
npm install --omit=dev <package_name>
```

## 1.2 Get the dependencies JSON dictionary

```
npm ls --all --long --omit=dev --json > npm_list.json 
```

Save the path of the file!

## 1.3 Execute the python script to get the list of dependencies enriched with size/loc and NPM registry metrics (license, ...)

1. Optional: create a python3 virtual env

```
python3 -m venv /tmp/vrdependencies 
source /tmp/vrdependencies/bin/activate
```

2. Install requirements

```
pip3 install -r requirements.txt
```

3. Execute the code
```
cd enrich_dependencies
python3 get_npm_dep_from_installed.py -npmlist npm_list.json -depoutput <newlist.json> -pjson <projects.json>
```


### Fix URLs that couldn't be found

If there is a repository that couldn't find its URL, the script will ask to write it manually (or leave it blank)

Example:
```
REPO URL NOT FOUND for pkgparent/pkg@version. Please, fill it out manually or leave it blank: https://github.com/repourl

```

### Returned files

Both ways will return a set of files:

- `newlist.json`: a simple 1D list with all the dependencies
- `projects.json`: project.json file for analysis with GrimoireLab



# 2. Prepare metrics

## 2.1 Community metrics

### Using grimoirelab docker images

1. Go to `grimoirelab-docker/default-grimoirelab-settings` and replace the content of `projects.json` file for the generated in the previous step.
2. Go to `grimoirelab-docker/compose` and launch the docker-compose, wait until all the git data is fetched, inside the /tmp/ folder the logs can be visited.


## 2.2 Vulnerability metrics

1. Run `npm audit --omit=dev --json > npm_audit.json` in the folder where the app/package is installed and save the output file path.


# 3. Enrich data with metrics

Once you have the CSV files with the community data, you have to go to the `enrich_dependencies` folder and:

**You can use the python venv created before**

1. Run the file
```
python3 enrich_dependencies.py 
-deplist <dependencies_list_json>
-depout <dependencies_list_enriches_output_path>
-auditfile <npm_audit_file>
-githubusername <github_username>
-githubtoken <github_token>
```

2. It will return the files in json format enriched with the community metrics, ready to use in a babia tree visualization