import argparse
import os
import sys
import time

import pandas as pd
import numpy as np
import requests
import json
from datetime import datetime
from dateutil.relativedelta import relativedelta
from pygount import SourceAnalysis, ProjectSummary
from glob import glob
from pathlib import Path
from elasticsearch import Elasticsearch

ES_HOST = "http://localhost:9200"
ES_INDEX = "git_enrich"
COMMUNITY_QUERY = {
  "aggs": {
    "repos": {
      "terms": {
        "field": "github_repo",
        "size": 500000,
        "order": {
          "max_commit_date": "desc"
        }
      },
      "aggs": {
        "lines_added": {
          "sum": {
            "field": "lines_added"
          }
        },
        "lines_changed": {
          "sum": {
            "field": "lines_changed"
          }
        },
        "lines_removed": {
          "sum": {
            "field": "lines_removed"
          }
        },
        "commiters": {
          "cardinality": {
            "field": "author_uuid"
          }
        },
        "max_commit_date": {
          "max": {
            "field": "commit_date"
          }
        },
        "min_commit_date": {
          "min": {
            "field": "commit_date"
          }
        },
        "files": {
          "sum": {
            "field": "files"
          }
        },
        "commits": {
          "cardinality": {
            "field": "hash"
          }
        }
      }
    }
  },
  "size": 0,
  "_source": {
    "excludes": []
  },
  "stored_fields": [
    "*"
  ],
  "script_fields": {
    "painless_inverted_lines_removed_git": {
      "script": {
        "source": "return doc['lines_removed'].value * -1",
        "lang": "painless"
      }
    }
  },
  "docvalue_fields": [
    {
      "field": "author_date",
      "format": "date_time"
    },
    {
      "field": "commit_date",
      "format": "date_time"
    },
    {
      "field": "demography_max_date",
      "format": "date_time"
    },
    {
      "field": "demography_min_date",
      "format": "date_time"
    },
    {
      "field": "grimoire_creation_date",
      "format": "date_time"
    },
    {
      "field": "metadata__enriched_on",
      "format": "date_time"
    },
    {
      "field": "metadata__timestamp",
      "format": "date_time"
    },
    {
      "field": "metadata__updated_on",
      "format": "date_time"
    },
    {
      "field": "utc_author",
      "format": "date_time"
    },
    {
      "field": "utc_commit",
      "format": "date_time"
    }
  ],
  "query": {
    "bool": {
      "must": [
        {
          "match_all": {}
        },
        {
          "match_all": {}
        },
        {
          "range": {
            "grimoire_creation_date": {
              "gte": 1348142971607,
              "lte": 1663660709792,
              "format": "epoch_millis"
            }
          }
        }
      ],
      "filter": [],
      "should": [],
      "must_not": []
    }
  }
}

def main():
    args = parse_args()
    
    auditf = open(args.audit_file)
    auditjson = json.load(auditf)
    
    # Opening JSON file
    f = open(args.dep_list)
    deplist = json.load(f)
    
    # Dependencies list
    deplist = add_metrics(deplist, auditjson, args.github_username,
                          args.github_token, args.dep_output)
    with open(args.dep_output, "w") as outfile:
        json.dump(deplist, outfile)
    

def parse_args():
    parser = argparse.ArgumentParser(usage="usage: enrich_dependencies.py [options]",
                                     description="Enrich metrics for representing the tree hierarchy")
    parser.add_argument("-deplist", "--dep-list", required=True,
                        help="Dependencies list file", default="./dependencieslist.json")
    parser.add_argument("-depout", "--dep-output", required=True,
                        help="Dependencies list file enriched", default="./dependencieslist_enriched_sh.json")
    parser.add_argument("-gitcsv", "--git-csv", required=False,
                        help="CSV with the git data from the dependencies", default="./git_sh.csv")
    parser.add_argument("-githubcsv", "--github-csv", required=False,
                        help="CSV with the github data from the dependencies", default="./github.csv")
    parser.add_argument("-gitlabmrscsv", "--gitlab-mrs-csv", required=False,
                        help="CSV with the gitlab mrs data from the dependencies", default="./gitlab_mrs.csv")
    parser.add_argument("-gitlabissuescsv", "--gitlab-issues-csv", required=False,
                        help="CSV with the gitlab issues data from the dependencies", default="./gitlab_issues.csv")
    parser.add_argument("-auditfile", "--audit-file", required=True,
                        help="Audit file of the dependencies for having the vulnerabilities",
                        default="./audit.json")
    parser.add_argument("-githubusername", "--github-username", required=True,
                        help="GitHub username for fetching the API",
                        default="dlumbrer")
    parser.add_argument("-githubtoken", "--github-token", required=True,
                        help="GitHub token for fetching the API",
                        default="xxxxx")
    
    return parser.parse_args()


def add_metrics(rawlist, auditjson, username, token, dep_output):
    
    
    # Vulnerabilities
    add_vulnerability_metrics(rawlist, auditjson)
    
    # Save before community cause it takes a long time
    with open(dep_output, "w") as outfile:
        json.dump(rawlist, outfile)
    
    # Community metrics from the GitHub API
    get_github_metrics(rawlist, username, token, (datetime.now() - relativedelta(years=1)))

    # Save before community cause it takes a long time
    with open(dep_output, "w") as outfile:
        json.dump(rawlist, outfile)

    # Community metrics
    add_community_from_es(rawlist)
    # add_community_from_cauldron(rawlist, gitdf, githubdf, gitlabmrsdf, gitlabissuesdf)
    
    # Calculate aggregations
    calculate_others(rawlist)
    
    
    return rawlist
    
    
def add_community_from_cauldron(rawlist, gitdf, githubdf, gitlabmrsdf, gitlabissuesdf):
    print("STARTING CAULDRON CSVS METRICS PROCESS")
    
    # Community
    for dep in rawlist:
        if 'repository' in dep:
            gitdf['repo_name'] = gitdf['repo_name'].str.lower()
            repogitdf = gitdf.loc[gitdf['repo_name'].str.endswith("{}.git".format(dep['repository']).lower())]
            # Commits
            dep['commits'] = repogitdf['hash'].nunique()
            # Maintainers - Commiters
            dep['maintainers'] = repogitdf['author_uuid'].nunique()
            # Date and files
            max_date = repogitdf['commit_date'].max()
            if isinstance(max_date, str):
                dep['last_commit'] = max_date
                last_commit_date = datetime.strptime(max_date, "%Y-%m-%dT%H:%M:%S")
                dep['seconds_from_last_commit'] = ((datetime.now() - last_commit_date).total_seconds()) * -1
                dep['hours_from_last_commit'] = ((datetime.now() - last_commit_date).total_seconds() // 3600) * -1
                dep['days_from_last_commit'] = (datetime.now() - last_commit_date).days * -1
                dep['last_act'] = (datetime.now() - last_commit_date).days * -1
                # dep['files'] = int(repogitdf.loc[repogitdf['commit_date'] == max_date]['files'].values[0])
            # Lines
            dep['lines_added'] = int(repogitdf['lines_added'].sum())
            dep['lines_removed'] = int(repogitdf['lines_removed'].sum())
            dep['files'] = int(repogitdf['files'].sum())
            
            # If Github or Gitlab
            # if 'github' in dep['repository']:
            #     githubdf['repository'] = githubdf['repository'].str.lower()
            #    repogithubdf = githubdf.loc[githubdf['repository'].str.endswith(dep['repository'].lower())]
            #    # Number of issues and Pull Requests
            #    dep['issues'] = repogithubdf[repogithubdf['pull_request'] == False].shape[0]
            #    dep['requests'] = repogithubdf[repogithubdf['pull_request'] == True].shape[0]
            #   # Contributors
            #   dep['contributors'] = repogithubdf[repogithubdf['pull_request'] == True]['author_uuid'].nunique()
            #   # Users - Issues
            #   dep['users'] = repogithubdf[repogithubdf['pull_request'] == False]['author_uuid'].nunique()
            # elif 'gitlab' in dep['repository']:
            #   gitlabissuesdf['repository'] = gitlabissuesdf['repository'].str.lower()
            #   repogitlabissuesdf = gitlabissuesdf.loc[
            #       gitlabissuesdf['repository'].str.endswith(dep['repository'].lower())]
            #   repogitlabmrsdf = gitlabmrsdf.loc[gitlabmrsdf['repository'].str.endswith(dep['repository'])]
                # Number of issues and Pull Requests
            #   dep['issues'] = repogitlabissuesdf.shape[0]
            #   dep['requests'] = repogitlabmrsdf.shape[0]
                # Contributors
            #   dep['contributors'] = repogitlabmrsdf['author_username'].nunique()
                # Users - Issues
            #   dep['users'] = repogitlabissuesdf['author_username'].nunique()
    
    return rawlist


def add_community_from_es(deplist):
    # Initialize
    for dep in deplist:
        dep['commits_alltime'] = 0
        dep['commiters_alltime'] = 0
        dep['last_act'] = 0
        dep['pkg_age'] = 0
        dep['commits_lastyear'] = 0
        dep['commiters_lastyear'] = 0
        dep['commits_last6m'] = 0
        dep['commiters_last6m'] = 0
    
    print("STARTING COMMUNITY ANALYSIS FROM ELASTICSEARCH AGG QUERY")
    COMMUNITY_QUERY["query"]["bool"]["must"][2]["range"]["grimoire_creation_date"]["lte"] = int(datetime.now().timestamp() * 1000)
    es = Elasticsearch([ES_HOST])
    res = es.search(index=ES_INDEX, body=COMMUNITY_QUERY)
    # print("Got %d Hits:" % res['hits']['total'])
    for hit in res['aggregations']['repos']['buckets']:
        for dep in deplist:
            if hit['key'] in dep['repository']:
                dep['commits_alltime'] = hit['commits']['value']
                dep['commiters_alltime'] = hit['commiters']['value']
                dep['last_act'] = hit['max_commit_date']['value']
                dep['pkg_age'] = hit['min_commit_date']['value']
                # Don't break because it can be repeated
                # break
    # Last year
    COMMUNITY_QUERY["query"]["bool"]["must"][2]["range"]["grimoire_creation_date"]["gte"] = int((datetime.now() - relativedelta(
        years=1)).timestamp() * 1000)
    res = es.search(index=ES_INDEX, body=COMMUNITY_QUERY)
    # print("Got %d Hits:" % res['hits']['total'])
    for hit in res['aggregations']['repos']['buckets']:
        for dep in deplist:
            if hit['key'] in dep['repository']:
                dep['commits_lastyear'] = hit['commits']['value']
                dep['commiters_lastyear'] = hit['commiters']['value']

    COMMUNITY_QUERY["query"]["bool"]["must"][2]["range"]["grimoire_creation_date"]["gte"] = int((datetime.now() - relativedelta(
        months=6)).timestamp() * 1000)
    res = es.search(index=ES_INDEX, body=COMMUNITY_QUERY)
    # print("Got %d Hits:" % res['hits']['total'])
    for hit in res['aggregations']['repos']['buckets']:
        for dep in deplist:
            if hit['key'] in dep['repository']:
                dep['commits_last6m'] = hit['commits']['value']
                dep['commiters_last6m'] = hit['commiters']['value']

def add_vulnerability_metrics(deplist, auditjson):
    print("STARTING VULNETABILITY METRICS PROCESS")
    # Initialize at 0
    # TODO: not efficient
    for res in deplist:
        res['nvuln'] = 0
        res['nvuln_critic'] = 0
        res['nvuln_high'] = 0
        res['nvuln_mod'] = 0
        res['nvuln_low'] = 0
        res['nvuln_info'] = 0
    
    # Check vulnerabilites
    if auditjson['vulnerabilities']:
        for pkg, value in auditjson['vulnerabilities'].items():
            # Find the dictionary of the vulnerability
            res = None
            for sub in deplist:
                if 'name' in sub and sub['name'] == pkg:
                    res = sub
                    break
            
            if res:
                # +1 to the total
                res['nvuln'] = res['nvuln'] + 1
                # +1 to the type
                if value['severity'] == "critical":
                    res['nvuln_critic'] = res['nvuln_critic'] + 1
                if value['severity'] == "high":
                    res['nvuln_high'] = res['nvuln_high'] + 1
                if value['severity'] == "moderate":
                    res['nvuln_mod'] = res['nvuln_mod'] + 1
                if value['severity'] == "low":
                    res['nvuln_low'] = res['nvuln_low'] + 1
                if value['severity'] == "info":
                    res['nvuln_info'] = res['nvuln_info'] + 1


def get_github_metrics(deplist, username, token, when):
    already_analyzed = {}
    now = datetime.now()
    print("STARTING GITHUB API METRICS PROCESS")
    for ix, dep in enumerate(deplist):
        if dep['repository'] not in already_analyzed:
            print("Getting GH Issues from {}, total {}/ {}".format(dep['id'], ix, len(deplist)))
            # Just do it one time and initialize
            if 'nprs' not in dep:
                dep['nprs'] = 0
                dep['nprs_closed'] = 0
                dep['nprs_lastyear'] = 0
                dep['nprs_closed_lastyear'] = 0
                dep['nprs_open'] = 0
                dep['nissues'] = 0
                dep['nissues_closed'] = 0
                dep['nissues_lastyear'] = 0
                dep['nissues_closed_lastyear'] = 0
                dep['nissues_open'] = 0
            else:
                # Means that the info is already there, so there is no need to query again
                continue
    
            # Check Rate Limit
            headers = {"Accept": "application/vnd.github+json", "Authorization": "Bearer {}".format(token)}
            x = requests.get('https://api.github.com/rate_limit', headers=headers)
            if x.status_code == 200:
                resp = x.json()
                if resp['resources']['search']['remaining'] < 10:
                    print("RATE LIMIT EXCEEDED, SLEEPING FOR 60 seconds")
                    time.sleep(60)
                    print("LETS TRY NOW!")
            else:
                print("Something wrong happened", x.status_code)
                exit(1)
            
            # Number of PRs
            x = requests.get('https://api.github.com/search/issues?q=repo:{}%20is:pr%20created:{}-{}-{}..{}-{}-{}&per_page=1'.format(
                dep['repository'].split("github.com/")[-1], when.year, when.month, when.day, now.year, now.month, now.day), auth=(username, token))
            if x.status_code == 200:
                resp = x.json()
                dep['nprs_lastyear'] = resp['total_count']
            else:
                print("Something wrong happened", x.status_code)

            x = requests.get(
                'https://api.github.com/search/issues?q=repo:{}%20is:pr&per_page=1'.format(
                    dep['repository'].split("github.com/")[-1], when.year, when.month, when.day, now.year, now.month,
                    now.day), auth=(username, token))
            if x.status_code == 200:
                resp = x.json()
                dep['nprs'] = resp['total_count']
            else:
                print("Something wrong happened", x.status_code)
            
            # Number of Closed PRs
            x = requests.get('https://api.github.com/search/issues?q=repo:{}%20is:pr%20is:closed%20created:{}-{}-{}..{}-{}-{}&per_page=1'.format(
                dep['repository'].split("github.com/")[-1], when.year, when.month, when.day, now.year, now.month, now.day), auth=(username, token))
            if x.status_code == 200:
                resp = x.json()
                dep['nprs_closed_lastyear'] = resp['total_count']
            else:
                print("Something wrong happened", x.status_code)

            x = requests.get(
                'https://api.github.com/search/issues?q=repo:{}%20is:pr%20is:closed&per_page=1'.format(
                    dep['repository'].split("github.com/")[-1], when.year, when.month, when.day, now.year, now.month,
                    now.day), auth=(username, token))
            if x.status_code == 200:
                resp = x.json()
                dep['nprs_closed'] = resp['total_count']
            else:
                print("Something wrong happened", x.status_code)
            
            # Number of Open PRs
            x = requests.get('https://api.github.com/search/issues?q=repo:{}%20is:pr%20is:open%20created:{}-{}-{}..{}-{}-{}&per_page=1'.format(
                dep['repository'].split("github.com/")[-1], when.year, when.month, when.day, now.year, now.month, now.day), auth=(username, token))
            if x.status_code == 200:
                resp = x.json()
                dep['nprs_open'] = resp['total_count']
            else:
                print("Something wrong happened", x.status_code)
            
            # Number of Issues
            x = requests.get('https://api.github.com/search/issues?q=repo:{}%20is:issue%20created:{}-{}-{}..{}-{}-{}&per_page=1'.format(
                dep['repository'].split("github.com/")[-1], when.year, when.month, when.day, now.year, now.month, now.day), auth=(username, token))
            if x.status_code == 200:
                resp = x.json()
                dep['nissues_lastyear'] = resp['total_count']
            else:
                print("Something wrong happened", x.status_code)

            x = requests.get('https://api.github.com/search/issues?q=repo:{}%20is:issue&per_page=1'.format(
                dep['repository'].split("github.com/")[-1]), auth=(username, token))
            if x.status_code == 200:
                resp = x.json()
                dep['nissues'] = resp['total_count']
            else:
                print("Something wrong happened", x.status_code)
            
            # Number of Closed Issues
            x = requests.get('https://api.github.com/search/issues?q=repo:{}%20is:issue%20is:closed%20created:{}-{}-{}..{}-{}-{}&per_page=1'.format(
                dep['repository'].split("github.com/")[-1], when.year, when.month, when.day, now.year, now.month, now.day), auth=(username, token))
            if x.status_code == 200:
                resp = x.json()
                dep['nissues_closed_lastyear'] = resp['total_count']

            x = requests.get(
                'https://api.github.com/search/issues?q=repo:{}%20is:issue%20is:closed&per_page=1'.format(
                    dep['repository'].split("github.com/")[-1], when.year, when.month, when.day, now.year, now.month,
                    now.day), auth=(username, token))
            if x.status_code == 200:
                resp = x.json()
                dep['nissues_closed'] = resp['total_count']
            
            # Number of Open Issues
            x = requests.get('https://api.github.com/search/issues?q=repo:{}%20is:issue%20is:open%20created:{}-{}-{}..{}-{}-{}&per_page=1'.format(
                dep['repository'].split("github.com/")[-1], when.year, when.month, when.day, now.year, now.month, now.day), auth=(username, token))
            if x.status_code == 200:
                resp = x.json()
                dep['nissues_open'] = resp['total_count']
            else:
                print("Something wrong happened", x.status_code)
                
            # Add to already analyzed in order to avoid to go into the GitHub API again
            already_analyzed[dep['repository']] = {
                'nprs': dep['nprs'],
                'nprs_closed': dep['nprs_closed'],
                'nprs_lastyear': dep['nprs_lastyear'],
                'nprs_closed_lastyear': dep['nprs_closed_lastyear'],
                'nprs_open': dep['nprs_open'],
                'nissues': dep['nissues'],
                'nissues_closed': dep['nissues_closed'],
                'nissues_lastyear': dep['nissues_lastyear'],
                'nissues_closed_lastyear': dep['nissues_closed_lastyear'],
                'nissues_open': dep['nissues_open'],
            }
        else:
            print("{} - {} already analyzed, not needed to go to the GitHub API".format(dep['repository'], dep['id']))
            dep['nprs'] = already_analyzed[dep['repository']]['nprs']
            dep['nprs_closed'] = already_analyzed[dep['repository']]['nprs_closed']
            dep['nprs_lastyear'] = already_analyzed[dep['repository']]['nprs_lastyear']
            dep['nprs_closed_lastyear'] = already_analyzed[dep['repository']]['nprs_closed_lastyear']
            dep['nprs_open'] = already_analyzed[dep['repository']]['nprs_open']
            dep['nissues'] = already_analyzed[dep['repository']]['nissues']
            dep['nissues_closed'] = already_analyzed[dep['repository']]['nissues_closed']
            dep['nissues_lastyear'] = already_analyzed[dep['repository']]['nissues_lastyear']
            dep['nissues_closed_lastyear'] = already_analyzed[dep['repository']]['nissues_closed_lastyear']
            dep['nissues_open'] = already_analyzed[dep['repository']]['nissues_open']
            
            
            # curl -H "Authorization: Bearer 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" 'https://api.github.com/search/issues?q=repo:desktop/desktop%20is:issue%20created<2022-01-01&per_page=1' ultimo añooo https://stackoverflow.com/questions/64300329/cant-you-specify-a-date-range-when-searching-the-repository-corresponding-to-to
            # requests.exceptions.ConnectionError: HTTPSConnectionPool(host='api.github.com', port=443): Max retries exceeded with url: /search/issues?q=repo:ljharb/internal-slot%20is:pr&per_page=1 (Caused by NewConnectionError('<urllib3.connection.HTTPSConnection object at 0x7f169646b220>: Failed to establish a new connection: [Errno -3] Temporary failure in name resolution'))
            
def calculate_others(deplist):
    for dep in deplist:
        if 'commiters_alltime' in dep and dep['commiters_alltime'] != 0:
            dep['commits_per_committer_alltime'] = dep['commits_alltime'] / dep['commiters_alltime']

        if 'commiters_lastyear' in dep and dep['commiters_lastyear'] != 0:
            dep['commits_per_committer_lastyear'] = dep['commits_lastyear'] / dep['commiters_lastyear']
            
        if 'loc' in dep and dep['loc'] != 0:
            dep['commit_per_loc'] = dep['commits_alltime'] / dep['loc']

        if 'commits_lastyear' in dep and dep['commits_lastyear'] != 0:
            dep['nissues_per_commit'] = dep['nissues'] / dep['commits_lastyear']
            
        if 'pkg_age' in dep and dep['pkg_age'] != 0:
            dt_object = datetime.fromtimestamp(int(dep['pkg_age'] / 1000))
            dep['age_days'] = (datetime.now() - dt_object).days
            dep['size/age'] = dep['size'] / dep['age_days']
            dep['loc/age'] = dep['loc'] / dep['age_days']
            dep['ncommits/age'] = dep['commits_alltime'] / dep['age_days']
            
        if 'last_act' in dep and dep['last_act'] != 0:
            dt_object = datetime.fromtimestamp(int(dep['last_act'] / 1000))
            dep['last_act_days'] = (datetime.now() - dt_object).days
            
        if 'commits_alltime' in dep and dep['commits_alltime'] != 0:
            dep['size_commits'] = dep['size'] / dep['commits_alltime']
            dep['loc_commits'] = dep['loc'] / dep['commits_alltime']
            
        if 'nissues' in dep and dep['nissues'] != 0:
            dep['nissues_ratio'] = dep['nissues_closed'] / dep['nissues']
        else:
            dep['nissues_ratio'] = 0
            
        if 'nissues_lastyear' in dep and dep['nissues_lastyear'] != 0:
            dep['nissues_ratio_lastyear'] = dep['nissues_closed_lastyear'] / dep['nissues_lastyear']
        else:
            dep['nissues_ratio_lastyear'] = 0
            
        # Renaming
        dep["ncommits"] = dep['commits_alltime']
        dep["ncommits_ly"] = dep['commits_lastyear']
        dep["ncommiters_ly"] = dep['commiters_lastyear']


if __name__ == '__main__':
    try:
        main()
    except KeyboardInterrupt:
        s = "\n\nReceived Ctrl-C or other break signal. Exiting.\n"
        sys.stdout.write(s)
        sys.exit(0)

