# Requirements

## Elevated City Scene

The scenes are web-based, and there's a permanent link to the repository deployed on GitLab Pages. You can **[click here to view the scene](https://thesis-dlumbrer.gitlab.io/vissoft23-experiment)**.

To deploy the scenes locally, you only need an HTTP server. Follow the steps outlined in the **[README.md/Scenes](./README.md#deploy-it-locally)** section to set it up.

## Hardware Needed

While the scenes are accessible on any device with a modern browser, to fully replicate the real scenario experienced by participants, we highly recommend using VR glasses with a modern browser, such as the Meta Quest 2. If VR glasses are not available, you can still explore the scenes using a regular computer.
