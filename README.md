# Understanding the NPM Dependencies Ecosystem of a Project Using Virtual Reality - Replication Package

## Table of Contents

A high level of this reproduction package doc.

- [Understanding the NPM Dependencies Ecosystem of a Project Using Virtual Reality - Replication Package](#understanding-the-npm-dependencies-ecosystem-of-a-project-using-virtual-reality---replication-package)
  - [Table of Contents](#table-of-contents)
  - [Scene](#scene)
    - [Deploy it locally](#deploy-it-locally)
  - [Projects description](#projects-description)
  - [Metrics description](#metrics-description)
  - [Analyzing a new project](#analyzing-a-new-project)
  - [Participants' questionnaire](#participants-questionnaire)
  - [Results data](#results-data)
  - [Video example](#video-example)

## Scene

[Go here to see the scene](https://thesis-dlumbrer.gitlab.io/vissoft23-experiment). Works in a desktop browser, but is intended for a
browser in a VR headset. The experiment was conducted with the browser
in Oculus Quest 2, immersed in VR.

### Deploy it locally

The easiest way to do it is to go to the `scene` folder and deploy the `scene/index.html` file within a simple HTTP server (i.e. node http-server or python simple http server).

(Optional) Examples of http servers deploy:
```shell
# Using node [1]
$> npm install -g http-server
$> http-server

# Using python 2 [2]
$> python -m SimpleHTTPServer

# Using python 3 [3]
$> python -m http.server
```

## Projects description

We have 4 projects to see in this scene, for switching between them there is the row of the User Interface called "Project":

- `ghdesktop`: Open source Electron-based GitHub app. It is written in TypeScript and uses React. 2391 dependencies in total.
- `portainer`: Lightweight service delivery platform for containerized applications that can be used to manage Docker, Swarm, Kubernetes, and ACI environments. 780 dependencies in total.
- `pm2`: Production process manager for Node.js applications with a built-in load balancer. 244 dependencies in total.
- `sortinghat-ui`: User interface for the Sortinghat application. 97 dependencies in total.

## Metrics description

These are the metrics that are available to change in the User Interface of the scene:

- `age_days`: package age in the total number of days.
- `loc/age`: lines of code of the package divided by the age in the total number of days.
- `size/age`: size in bytes of the package divided by the age in the total number of days.
- `ncommits/age`: number of commits of the repository of the package divided by the age in the total number of days.
- `license`: type of license of the package.
- `timesInstalled`: how many times the package is installed, that means, how many different versions the package is in the project.
- `timesAppear`: how many times the package appears in the project regardless of the version.
- `last_act_days`: days since the last commit in the repository of the package.
- `ncommits_ly`: number of commits in the repository of the package in the last year.
- `ncommiters_ly`: number of unique committers in the repository of the package in the last year.
- `nvuln`: number of vulnerabilities of the package, retrieved using the `audit` tool of npm.
- `nissues_ratio`: the number of issues closed divided by the total number of issues.

## Analyzing a new project

This replication package also includes the possibility of analyzing a custom node project. For retrieving the needed data for creating the visualization, you need to follow the tutorial inside the `.scripts/README.md`.

[Click here to visit the tutorial](./scripts/README.md)

## Participants' questionnaire

Inside the `questionnaire` folder are the documents followed by the experimenter along with the questionnaire presented to the participants:

- [Layout overview](./questionnaire/overview.pdf)
- [Training and Feedback questions](./questionnaire/trainingfeedback.pdf)


## Results data

In the `results` folder are the CSV files of the answers (raw and formatted) of the participants:

- `results/insights.csv`: Contains the raw answers of the screen participants. [Click here to go to the file](./results/insights.csv)
- `results/demofeedback.csv`: Contains the formatted demographics answers of the screen participants. [Click here to go to the file](./results/demofeedback.csv)


This is the meaning of each column title:

- PARTICIPANT: ID of the participant
- VIEW: The view selected.
- VIEW COMB 2: If a second view was used to retrieve the information.
- VIEW COMB 3: If a third view was used to retrieve the information.
- AREA: Metric selected for the area.
- COLOR: Metric selected for the color.
- COLOR 2: If a second metric for the color was used to retrieve the information.
- COLOR 3: If a third metric for the color was used to retrieve the information.
- TRANSPARENT: If the feature of transparency was enabled.
- WIREFRAME: If the feature of the wireframe was enabled.
- INFO: The information extracted from the metrics.
- EXP VR: If the participant has previous experience with VR devices.
- EXP NPM: If the participant has previous experience with NPM.
- EXP PRG: years of experience in programming.
- F1: Response to the `What do you find the most useful view (or combination of views)?` question.
- F2: Response to the `What do you find the most useful metrics?` question.
- F3: Response to the `What parts/features of the metaphor did you find more useful?` question.
- F4: Response to the `Is there another tool that you use for analyzing these dependencies metrics?` question.
- F5: Response to the `Would you change/improve something about the tool?` question.
- F6: Response to the `Do you have any other comments?` question.
- NOTES: Additional notes of the experimenter.



## Video example

Sample video of one of the participants during the experiment (audio removed for privacy reasons).

- [Video example](./videos/participant.mp4)